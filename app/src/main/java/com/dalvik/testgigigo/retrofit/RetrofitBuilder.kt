package com.dalvik.testgigigo.retrofit

import com.dalvik.testgigigo.utils.Constants
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
  private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
    }

    private val client = OkHttpClient.Builder()
        .addInterceptor(HeaderInterceptor()).build()



    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}