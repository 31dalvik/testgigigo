package com.dalvik.testgigigo.retrofit

import com.dalvik.testgigigo.BuildConfig
import com.dalvik.testgigigo.utils.Constants
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain
            .request()
            .url()
            .newBuilder()
            .addQueryParameter(Constants.PARAMETER_API_KEY, BuildConfig.GOOGLE_API_KEY)
            .build()
        return  chain.proceed(chain.request().newBuilder().url(url).build())
    }
}