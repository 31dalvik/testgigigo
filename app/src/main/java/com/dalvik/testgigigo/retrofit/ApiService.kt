package com.dalvik.testgigigo.retrofit

import com.dalvik.testgigigo.entities.ResponseRouteGoogle
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("json?")
    fun getRoute(
        @Query("origin") origin: String,
        @Query("destination") destination: String
    ): Call<ResponseRouteGoogle>

}