package com.dalvik.testgigigo.utils


import androidx.multidex.MultiDexApplication
import com.dalvik.testgigigo.BuildConfig
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.net.PlacesClient


class LocationApplication: MultiDexApplication()  {

    override fun onCreate() {
        super.onCreate()
        instance = this
        inicializateLocation()

    }

    companion object{
        lateinit var instance: LocationApplication
        lateinit var  instancePlaces: PlacesClient
    }

    private fun inicializateLocation(){
        if (!Places.isInitialized()) {
            Places.initialize(applicationContext, BuildConfig.GOOGLE_API_KEY)
        }
        instancePlaces = Places.createClient(applicationContext)

    }
}