package com.dalvik.testgigigo.utils

class Constants {
    companion object {
        const val BASE_URL = "https://maps.googleapis.com/maps/api/directions/"
        const val ZOOM_MAP = 16.0F
        const val PARAMETER_API_KEY = "key"
        const val ID_LOCATION = "ID_LOCATION"


    }
}