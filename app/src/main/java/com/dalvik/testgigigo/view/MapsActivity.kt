package com.dalvik.testgigigo.view

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.dalvik.locationmanagement.LocationManagement
import com.dalvik.testgigigo.R
import com.dalvik.testgigigo.databinding.ActivityMapsBinding
import com.dalvik.testgigigo.utils.Constants
import com.dalvik.testgigigo.viewmodel.LocationViewModel
import com.dalvik.testgigigo.viewmodel.LocationViewModelFactory
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.google.maps.android.PolyUtil


class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapsBinding
    private lateinit var locationViewModel: LocationViewModel
    private lateinit var locationManagement: LocationManagement
    private lateinit var locationStart: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMapsBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initConfig()

    }


    private fun initConfig() {
        locationManagement = LocationManagement.from(this)
        locationViewModel =
            ViewModelProvider(
                this,
                LocationViewModelFactory(locationManagement, supportFragmentManager)
            ).get(
                LocationViewModel::class.java
            )
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        locationViewModel.configFragmentAutocomplete()
        getPlaceSelected()
    }

    @SuppressLint("PotentialBehaviorOverride")
    private fun getPlaceSelected() {
        locationViewModel.getPlaceSelected { placeId, ltnlng ->
            if (placeId != null) {
                locationViewModel.getRouteGoogle(
                    locationStart,
                    "place_id:${placeId}"
                ) { responseLocation, error ->

                    if (!error.isNullOrEmpty()) {
                        Toast.makeText(this, error, Toast.LENGTH_SHORT)
                            .show()
                    } else {
                        mMap.clear()
                        var polylineOptions = PolylineOptions()
                        mMap.addMarker(
                            MarkerOptions().position(ltnlng!!)
                        )
                        mMap.setOnMarkerClickListener{
                            if(it!= null){
                                val intent = Intent(this, DetailActivity::class.java).apply {
                                    putExtra(Constants.ID_LOCATION,placeId)
                                }
                                startActivity(intent)
                            }
                            false
                        }


                        mMap.moveCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                ltnlng,
                                Constants.ZOOM_MAP
                            )
                        )
                        polylineOptions.addAll(PolyUtil.decode(responseLocation))
                        mMap.addPolyline(polylineOptions)

                    }
                }
            } else {
                Toast.makeText(this, getString(R.string.msg_error_placeid), Toast.LENGTH_SHORT)
                    .show()
            }

        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        getCurrentLocation()
    }

    @SuppressLint("MissingPermission")
    fun getCurrentLocation() {
        locationViewModel.getLocation { location ->
            if (location != null) {
                mMap.isMyLocationEnabled = true
                locationStart = "${location.latitude},${location.longitude}"
                val currentLocation = LatLng(location.latitude, location.longitude)
                mMap.moveCamera(
                    CameraUpdateFactory.newLatLngZoom(
                        currentLocation,
                        Constants.ZOOM_MAP
                    )
                )
            }
        }
    }
}