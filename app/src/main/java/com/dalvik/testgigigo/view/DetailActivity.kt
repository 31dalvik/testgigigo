package com.dalvik.testgigigo.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.dalvik.testgigigo.R
import com.dalvik.testgigigo.databinding.ActivityDetailBinding
import com.dalvik.testgigigo.utils.Constants
import com.dalvik.testgigigo.viewmodel.LocationViewModel
import com.dalvik.testgigigo.viewmodel.LocationViewModelFactory

class DetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDetailBinding
    private lateinit var viewModel: LocationViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        viewModel = ViewModelProvider(
            this,
            LocationViewModelFactory(null, supportFragmentManager)
        ).get(
            LocationViewModel::class.java
        )
        getElementsExtra()
    }

    private fun getElementsExtra() {
        val locationId = intent.getStringExtra(Constants.ID_LOCATION)

        viewModel.getPlaceDetails(locationId) { place, bitmap, error ->
            if (error.isNullOrEmpty()) {
                if (supportActionBar != null) {
                    supportActionBar?.title = place!!.name
                }
                binding.place = place
                binding.imgLocation.setImageBitmap(bitmap)
                binding.tvCoordenadas.text =
                    "lat: ${place!!.latLng.latitude}, lng: ${place.latLng.longitude}"
            } else {
                Toast.makeText(this, error, Toast.LENGTH_SHORT)
                    .show()
            }
        }
    }
}