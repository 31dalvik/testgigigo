package com.dalvik.testgigigo.viewmodel

import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.dalvik.locationmanagement.LocationManagement

class LocationViewModelFactory(private var locationManagement: LocationManagement?, private var fragmentManager: FragmentManager): ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(LocationViewModel::class.java)) {
            return LocationViewModel(locationManagement, fragmentManager) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}