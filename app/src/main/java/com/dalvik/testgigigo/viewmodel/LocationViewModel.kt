package com.dalvik.testgigigo.viewmodel

import android.graphics.Bitmap
import android.location.Location
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModel
import com.dalvik.locationmanagement.LocationManagement
import com.dalvik.testgigigo.repository.LocationRepository
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.Place

class LocationViewModel(var locationManager: LocationManagement?, fragmentManager: FragmentManager): ViewModel() {

    private var locationRepository: LocationRepository =
        LocationRepository.getInstance(locationManager,fragmentManager)

    fun getLocation(
        onResult: (response: Location?) -> Unit
    ) {
        locationRepository.getLocation { responseLocation ->
            onResult(responseLocation)
        }
    }


    fun getRouteGoogle(
        locationStart: String,
        locationEnd: String,
        onResult: (response: String?, error: String?) -> Unit
    ) {
        locationRepository.getRouteGoogle(locationStart,locationEnd) { responseLocation,error ->
            onResult(responseLocation,error)
        }
    }

    fun configFragmentAutocomplete(){
        locationRepository.configFragmentAutocomplete()
    }


    fun getPlaceSelected(
        onResult: (placeId: String?, latlng: LatLng?) -> Unit
    ){
        locationRepository.getPlaceSelected{placeId, latlng ->
            onResult(placeId,latlng)
        }
    }

    fun getPlaceDetails(
        placeId: String?,
        onResult: (place: Place?, imgLocation: Bitmap?, error: String?) -> Unit
    ){
        locationRepository.getPlaceDetail(placeId!!){place, bitmap, error ->
            onResult(place,bitmap,error)
        }
    }
}