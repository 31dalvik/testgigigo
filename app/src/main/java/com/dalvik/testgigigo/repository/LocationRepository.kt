package com.dalvik.testgigigo.repository

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.location.Location
import androidx.fragment.app.FragmentManager
import com.dalvik.locationmanagement.LocationManagement
import com.dalvik.testgigigo.R
import com.dalvik.testgigigo.entities.ResponseRouteGoogle
import com.dalvik.testgigigo.retrofit.RetrofitBuilder
import com.dalvik.testgigigo.utils.LocationApplication
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.net.*
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocationRepository(
    private var localtionManager: LocationManagement?,
    private var fragmentManager: FragmentManager
) {
    private lateinit var autocompleteFragment: AutocompleteSupportFragment

    companion object {
        private var INSTANCE: LocationRepository? = null
        fun getInstance(localtionManager: LocationManagement?, fragmentManager: FragmentManager) =
            INSTANCE ?: LocationRepository(localtionManager, fragmentManager).also {
                INSTANCE = it
            }
    }


    @SuppressLint("NewApi")
    fun getLocation(
        onResult: (response: Location?) -> Unit
    ) {
        localtionManager?.messagePermission(LocationApplication.instance.getString(R.string.message_permission_location))
            ?.messageObtainLocation(LocationApplication.instance.getString(R.string.msg_obtain_location_permissions))
            ?.colorProgress(R.color.purple_200)
            ?.isLocationTracking(false)?.getLastLocation {
                onResult(it)
            }

    }


    fun getRouteGoogle(
        locationStart: String,
        locationEnd: String,
        onResult: (response: String?, error: String?) -> Unit
    ) {

        RetrofitBuilder.apiService.getRoute(locationStart, locationEnd)
            .enqueue(object :
                Callback<ResponseRouteGoogle> {
                override fun onResponse(
                    call: Call<ResponseRouteGoogle>,
                    response: Response<ResponseRouteGoogle>
                ) {

                    if (response.body() != null && !response.body()?.routes.isNullOrEmpty() && response.body()!!.routes[0].overviewPolyline != null && !response.body()!!.routes[0].overviewPolyline!!.points.isNullOrEmpty()) {
                        onResult(response.body()!!.routes[0].overviewPolyline?.points, null)
                    } else {
                        onResult(
                            null,
                            LocationApplication.instance.getString(R.string.msg_error_route_google)
                        )
                    }

                }

                override fun onFailure(call: Call<ResponseRouteGoogle>, t: Throwable) {
                    onResult(null, t.message)
                }

            })
    }


    fun configFragmentAutocomplete() {
        autocompleteFragment =
            fragmentManager.findFragmentById(R.id.place_autocomplete_fragment)
                    as AutocompleteSupportFragment

        autocompleteFragment.setPlaceFields(
            listOf(
                Place.Field.ID,
                Place.Field.LAT_LNG
            )
        )

    }


    fun getPlaceSelected(
        onResult: (placeId: String?, latlng: LatLng?) -> Unit
    ) {
        autocompleteFragment.setOnPlaceSelectedListener(object : PlaceSelectionListener {
            override fun onPlaceSelected(place: Place) {
                if (!place.id.isNullOrEmpty()) {
                    onResult(place.id, place.latLng)
                } else {
                    onResult(null, null)
                }
            }

            override fun onError(status: Status) {
                onResult(status.statusMessage, null)
            }
        })
    }


    fun getPlaceDetail(
        placeId: String,
        onResult: (place: Place?, imgLocation: Bitmap?, error: String?) -> Unit
    ) {
        val placeFields = listOf(Place.Field.ID, Place.Field.NAME,Place.Field.LAT_LNG,Place.Field.ADDRESS ,Place.Field.PHOTO_METADATAS)
        val request = FetchPlaceRequest.newInstance(placeId, placeFields)

        LocationApplication.instancePlaces.fetchPlace(request)
            .addOnSuccessListener { response: FetchPlaceResponse ->

                if(response.place!=null){
                    val metada = response.place.photoMetadatas

                    if (metada == null || metada.isEmpty()) {
                        onResult(response.place, null,null)
                        return@addOnSuccessListener
                    }
                    val photoMetadata = metada.first()
                    val photoRequest = FetchPhotoRequest.builder(photoMetadata)
                        .setMaxWidth(500)
                        .setMaxHeight(300)
                        .build()
                    LocationApplication.instancePlaces.fetchPhoto(photoRequest)
                        .addOnSuccessListener { fetchPhotoResponse: FetchPhotoResponse ->
                            val bitmap = fetchPhotoResponse.bitmap
                            onResult(response.place, bitmap,null)
                        }.addOnFailureListener { exception: Exception ->
                            if (exception is ApiException) {
                                onResult(response.place, null,exception.message)
                            }
                        }
                }
            }.addOnFailureListener { exception: Exception ->
                if (exception is ApiException) {
                    onResult(null,null, exception.toString())
                }
            }
    }



}