package com.dalvik.testgigigo.entities

import com.google.gson.annotations.SerializedName

data class ResponseRouteGoogle(
    @SerializedName("routes")
    val routes: List<Routes>
)
