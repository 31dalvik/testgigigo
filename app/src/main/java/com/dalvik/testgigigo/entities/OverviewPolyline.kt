package com.dalvik.testgigigo.entities

import com.google.gson.annotations.SerializedName

data class OverviewPolyline(
    @SerializedName("points" ) var points : String? = null
)
