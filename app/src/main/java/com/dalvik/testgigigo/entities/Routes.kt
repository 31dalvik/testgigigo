package com.dalvik.testgigigo.entities

import com.google.gson.annotations.SerializedName

data class Routes(
    @SerializedName("overview_polyline" ) var overviewPolyline : OverviewPolyline? = OverviewPolyline()
)
